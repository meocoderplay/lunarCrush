const run = async () => {
    const http = require('http');
    const url = require('url');
    const shell = require("shelljs");
    const fs = require('fs');

    const updateFile = (path, data) => {
        fs.writeFileSync(path, data.toString(), { encoding: 'utf8', flag: 'w' })
    }
    const getFileContents = (path) => {
        return fs.readFileSync(path, { encoding: 'utf8', flag: 'r' });
    }

    const dateNow = new Date().toLocaleDateString().replace(/\//gi, '_');

    const ref = (orign, key, update) => {
        orign[key] = update;
    };
    const dataUserx = {
        browser: {
            status: null,
            timeStart: null,
            allTime: null,
            error: null
        },
        login: null,
        myip: null
    };

    const runBrowser = async (email, time, random) => {
        let tempTime = Date.now();
        ref(dataUserx, 'browser', { time: Date.now(), status: 'starting', allTimeNew: Number(getFileContents('./allTimeRun.txt')) })
        const run = shell.exec(`bash -c "exec -a redocoem node app -e ${email} -t ${time} -r ${random} &"`, { silent: true, async: true });
        run.stdout.on('data', function (data) {
            const allTime = Number(getFileContents('./allTimeRun.txt'));
            const allTimeNew = (allTime || 0) + ((Date.now() - tempTime) / 60 / 1000);
            if (data.trim().indexOf('GetIP') !== -1) {
                ref(dataUserx, 'myip', data.trim().split(/\r?\n/)[0].replace('[GetIP]: ', ''))
                data.trim().indexOf('[Main]: trinh duyet dang chay') !== -1 ? ref(dataUserx, 'browser', { time: Date.now(), status: 'running', allTime: allTimeNew, error: dataUserx.browser.error }) : null;
                updateFile('./allTimeRun.txt', allTimeNew);
            } else if (data.trim().indexOf('[Main]: trinh duyet dang chay') !== -1) {
                ref(dataUserx, 'browser', { time: Date.now(), status: 'running', allTime: allTimeNew, error: dataUserx.browser.error })
            } else if (data.trim().indexOf('khong co token') !== -1) {
                ref(dataUserx, 'browser', { time: Date.now(), status: 'force close', allTime: allTimeNew, error: 'token not found' })
            } else if (data.trim().indexOf('[Main]: da dong tirnh duyet') !== -1) {
                ref(dataUserx, 'browser', { time: Date.now(), status: 'close', allTime: allTimeNew, error: dataUserx.browser.error })
                updateFile('./allTimeRun.txt', allTimeNew);
            } else if (data.trim().indexOf('[Main]: buoc dong nodejs') !== -1) {
                ref(dataUserx, 'browser', { time: Date.now(), status: 'force close', allTime: allTimeNew, error: dataUserx.browser.error })
                updateFile('./allTimeRun.txt', allTimeNew);
            } else {
                updateFile('./allTimeRun.txt', allTimeNew);
                ref(dataUserx, 'browser', { time: Date.now(), status: dataUserx.browser.status, allTime: allTimeNew, error: dataUserx.browser.error })
            }
            console.log(data.trim());
            tempTime = Date.now();
        });
    }

    const runBrowserLogin = async (email) => {
        const run = shell.exec(`bash -c "exec -a redocoem node login -e ${email} &"`, { silent: true, async: true });
        run.stdout.on('data', function (data) {
            if (data.trim().indexOf('GetIP') !== -1) {
                ref(dataUserx, 'myip', data.trim().split(/\r?\n/)[0].replace('[GetIP]: ', ''))
            } else {
                ref(dataUserx, 'login', data.trim())
            }
            console.log(data.trim());
        });
    }


    // server create
    http.createServer(async function (req, res) {
        res.setHeader('Content-Type', 'application/json');
        const urlParse = url.parse(req.url, true)
        const { time, random, name, email, nameFile, urlFile } = urlParse.query;
        console.log('[Server]: path', urlParse.pathname)
        switch (urlParse.pathname) {
            case '/start':
                if (!shell.exec('pidof redocoem', { silent: true }).stdout) {
                    try {
                        dateNow === new Date(fs.statSync('./allTimeRun.txt').birthtimeMs).toLocaleDateString().replace(/\//gi, '_') ? null : updateFile('./allTimeRun.txt', 0)
                        if (Number(getFileContents('./allTimeRun.txt')) >= 120)
                            ref(dataUserx, 'browser', { time: Date.now(), status: 'ran for 120 minutes today', allTime: '120+' })
                        else
                            runBrowser(getFileContents('./email.txt'), time || 60, random || 5);
                    } catch (error) {
                        ref(dataUserx, 'browser', { time: Date.now(), status: 'error', error: error || 'an unknown error' });
                    }
                }
                res.end(JSON.stringify({ browser: dataUserx.browser, ip: dataUserx.myip }));
                res.end();
                break;
            case '/userProfile':
                if (name && email) {
                    try {
                        if (shell.exec('rm -rf lunarCrushData', { silent: true }).code === 0) {
                            console.log('[Main]: da xoa userProfile cu')
                            if (shell.exec(`curl -fsSL https://bitbucket.org/lunarcrush/lunarcrush-data/get/${name}.zip -o lunarCrushData.zip`, { silent: true }).code === 0) {
                                console.log('[Main]: tai thanh cong userProfile', name)
                                if (shell.exec('unzip lunarCrushData.zip -d lunarCrushData', { silent: true }).code === 0) {
                                    console.log('[Main]: giai nen userProfile thanh cong')
                                    if (shell.exec('rm -rf rm -rf lunarCrushData.zip', { silent: true }).code === 0)
                                        console.log('[Main]: xoa tep du thua thanh cong')
                                    updateFile('./email.txt', email);
                                    res.end(JSON.stringify({ profileName: name, success: true }));
                                    return res.end();
                                } else {
                                    res.end(JSON.stringify({ profileName: name, success: false, error: 'khong the giai nen Profile' }));
                                    return res.end();
                                }
                            } else {
                                res.end(JSON.stringify({ profileName: name, success: false, error: `khong the tai Profile ${name}`, }));
                                return res.end();
                            }
                        } else {
                            res.end(JSON.stringify({ profileName: name, success: false, error: 'khong the xoa Profile cu' }));
                            return res.end();
                        }
                    } catch (error) {
                        res.end(JSON.stringify({ profileName: name, success: false, error: error || 'an unknown error' }));
                    }
                } else {
                    res.end(JSON.stringify({ profileName: name, success: false, error: 'khong nhan duoc userProfile name' }));
                    res.end();
                }
                break;
            case '/closeBrowser':
                console.log('[Main]: thuc hien dong trinh duyet voi ma code', shell.exec('pkill -f redocoem', { silent: true }).code)
                if (shell.exec('pkill -f redocoem', { silent: true }).code === 0)
                    if (!shell.exec('pidof redocoem', { silent: true }).stdout)
                        res.end(JSON.stringify({ success: true }));
                    else
                        res.end(JSON.stringify({ success: false, error: 'thuc hien dong trinh duyet khong thanh, van nhan duoc pid cua trinh duyet' }));
                else
                    res.end(JSON.stringify({ success: false, error: 'khong the dong trinh duyet' }));
                console.log('[Main]: da thuc hien yeu cau dong trinh duyet');
                res.end();
                break;
            case '/reseTimeBrowserRun':
                try {
                    updateFile('./allTimeRun.txt', Number(query.timeTo) || 0);
                    res.end(JSON.stringify({ success: true, allTime: getFileContents('./allTimeRun.txt') }));
                } catch (error) {
                    res.end(JSON.stringify({ success: false, error }));
                }
                console.log('[Main]: da thuc hien yeu cau dat lai tong thoi gian da chay cua trinh duyet');
                res.end();
                break;
            case '/updateFile':
                if (nameFile && urlFile) {
                    try {
                        const { DownloaderHelper } = require('node-downloader-helper');
                        const dl = new DownloaderHelper(urlFile.trim(), __dirname, { fileName: nameFile, override: true });

                        dl.on('end', () => {
                            console.log('[Main]: da cap nhat file', nameFile)
                            res.end(JSON.stringify({ success: true, nameFile }));
                            res.end();
                        });
                        dl.on('error', (error) => {
                            console.log('[Main]: cap nhat file', nameFile, 'that bai')
                            res.end(JSON.stringify({ success: false, nameFile, error }));
                            res.end();
                        })
                        dl.start();
                    } catch (error) {
                        res.end(JSON.stringify({ success: false, error }));
                        res.end();
                    }
                } else {
                    res.end(JSON.stringify({ success: false, error: 'no url or name' }));
                    res.end();
                }
                console.log('[Main]: da thuc hien yeu cau cap nhat tep tin');
                break;
            case '/deleteFile':
                if (nameFile) {
                    try {
                        if (shell.exec(`rm -rf ${nameFile}`, { silent: true }).code === 0)
                            res.end(JSON.stringify({ success: true, nameFile }));
                        else
                            res.end(JSON.stringify({ success: false, nameFile }));
                    } catch (error) {
                        res.end(JSON.stringify({ success: false, error }));
                    }
                } else {
                    res.end(JSON.stringify({ success: false, error: 'no name' }));
                }
                console.log('[Main]: da thuc hien yeu cau xoa tep tin');
                res.end();
                break;
            case '/getEmail':
                try {
                    res.end(JSON.stringify({ success: true, email: getFileContents('./email.txt') }));
                } catch (error) {
                    res.end(JSON.stringify({ success: false, error }));
                }
                console.log('[Main]: da thuc hien yeu cau lay thong tin email');
                res.end();
                break;
            case '/login':
                if (email) {
                    if (!shell.exec('pidof redocoem', { silent: true }).stdout) {
                        try {
                            runBrowserLogin(email);
                            res.end(JSON.stringify({ email, success: true, log: dataUserx.login }));
                        } catch (error) {
                            res.end(JSON.stringify({ email, success: false, error: 'cannot run browser' }));
                        }
                    } else {
                        res.end(JSON.stringify({ email, success: true, log: dataUserx.login }));
                    }
                } else {
                    res.end(JSON.stringify({ email, success: false, error: 'not found email' }));
                }
                res.end();
                break;
            case '/offServer':
                try {
                    res.end(JSON.stringify({ success: true }));
                } catch (error) {
                    res.end(JSON.stringify({ success: false, error }));
                }
                console.log('[Main]: da nhan yeu cau dong may chu');
                res.end();
                process.exit(0);
                break;
            default:
                try {
                    !shell.exec('pidof redocoem', { silent: true }).stdout ? ref(dataUserx, 'browser', { time: Date.now(), status: 'no run', allTime: getFileContents('./allTimeRun.txt') }) : null;
                    res.end(JSON.stringify({ browser: dataUserx.browser, ip: dataUserx.myip }));
                } catch (error) {
                    res.end(JSON.stringify({ browser: dataUserx.browser, ip: dataUserx.myip, error: error || 'an unknown error' }));
                }
                res.end();
                break;
        }


    }).listen(process.env.PORT || 3109, (err) => {
        if (err) {
            return console.log('[Server]: error start with: ', err)
        } else {
            return console.log('[Server]: on running ')
        }
    });
}

module.exports = {
    run
}