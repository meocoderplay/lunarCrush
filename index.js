const axios = require("axios");
const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
puppeteer.use(StealthPlugin());
const blockResourcesPlugin =
  require("puppeteer-extra-plugin-block-resources")();
blockResourcesPlugin.blockedTypes.add("image");
blockResourcesPlugin.blockedTypes.add("media");
blockResourcesPlugin.blockedTypes.add("font");
blockResourcesPlugin.blockedTypes.add("manifest");
puppeteer.use(blockResourcesPlugin);

const args = process.argv.slice(2);

const options = {
  width: Math.floor(Math.random() * 370) + 350,
  height: Math.floor(Math.random() * 220) + 850,
};

const listLinks = ["https://lunarcrush.com"];

const listLinksReload = [
  "https://lunarcrush.com/global?interval=1d",
  "https://lunarcrush.com/global",
  "https://lunarcrush.com/global?interval=1m",
  "https://lunarcrush.com/redirect?from=cotd",
  "https://lunarcrush.com/portfolio",
  "https://lunarcrush.com/global?metric=social_score",
  "https://lunarcrush.com/global?metric=social_contributors",
  "https://lunarcrush.com/global?metric=url_shares",
  "https://lunarcrush.com/global?metric=news",
  "https://lunarcrush.com/global?metric=btc_dominance",
  "https://lunarcrush.com/global?metric=altcoin_dominance",
  "https://lunarcrush.com/global?metric=tweet_spam",
  "https://lunarcrush.com/global?metric=market_cap",
  "https://lunarcrush.com/global?metric=volume",
  "https://lunarcrush.com/markets",
  "https://lunarcrush.com/markets?metric=correlation_rank",
  "https://lunarcrush.com/markets?metric=galaxy_score",
  "https://lunarcrush.com/markets?metric=alt_rank",
  "https://lunarcrush.com/markets?metric=time_added",
  "https://lunarcrush.com/influencers",
  "https://lunarcrush.com/exchanges?metric=alt_rank",
  "https://lunarcrush.com/exchanges?metric=1d_trades",
  "https://lunarcrush.com/exchanges?metric=1d_volume_percent",
];

const userAgent = [
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0",
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36",
];
const headers = {
  Accept: "*/*",
  "Accept-Encoding": "gzip, deflate, br",
  "Accept-Language":
    "en-US,en;q=0.9,fr;q=0.8,ro;q=0.7,ru;q=0.6,la;q=0.5,pt;q=0.4,de;q=0.3",
  "Cache-Control": "max-age=0",
  Connection: "keep-alive",
  "Upgrade-Insecure-Requests": "1",
  "User-Agent": userAgent[Math.floor(Math.random() * userAgent.length)],
};

const timeRun = (Math.floor(Math.random() * 15) + 275) * 60 * 1000;
const timeStart = Date.now();
axios
  .get(
    `https://api.lunarcrush.com/v2?data=user&action=points-detail&key=${args[0]}`,
    {
      headers: headers,
    }
  )
  .then(function (response) {
    if (response.status === 200) {
      response.data.data.metadata["total_point"] = response.data.data.points;
      console.table(response.data.data.metadata);
    }
  })
  .catch(function (error) {
    console.log("loi");
  });

const getDetailsPoints = setInterval(() => {
  axios
    .get(
      `https://api.lunarcrush.com/v2?data=user&action=points-detail&key=${args[0]}`,
      {
        headers: headers,
      }
    )
    .then(function (response) {
      if (response.status === 200) {
        response.data.data.metadata["total_point"] = response.data.data.points;
        console.table(response.data.data.metadata);
      }
    })
    .catch(function (error) {
      console.log("loi");
    });
  if (300000 > timeRun - (Date.now() - timeStart)) {
    clearInterval(getDetailsPoints);
  }
}, 300000);

puppeteer
  .launch({
    userDataDir: "./lunarCrushData",
    headless: true,
    args: [
      "--no-sandbox",
      "--disable-setuid-sandbox",
      `--window-size=${options.width},${options.height}`,
    ],
  })
  .then(async (browser) => {
    console.log("[Main]: trinh duyet dang chay :))");
    //const timeRun = (Math.floor(Math.random() * 15) + 0) * 60 * 1000;
    console.log(`[Main]: trinh duyet se dong sau ${timeRun / 60 / 1000} phut`);
    setTimeout(async () => {
      await browser.close();
      console.log("[Main]: da dong tirnh duyet");
    }, timeRun);
    const page = await browser.newPage();
    await page.goto(listLinks[Math.floor(Math.random() * listLinks.length)], {
      waitUntil: "load",
      timeout: 0,
    });
    setInterval(async () => {
      console.log(`[${new Date().toLocaleString()}]: reload page!`);
      await page.goto(
        listLinksReload[Math.floor(Math.random() * listLinksReload.length)],
        {
          waitUntil: "load",
          timeout: 0,
        }
      );
    }, 900000);
    await page.evaluate(() => {
      setInterval(() => {
        if (
          document.body.textContent ===
          'Are you a human?Failed to verify that you are a human on planet earth. If you are really a human you can try again in a minute.Try Again{"props":{"pageProps":{}},"page":"/","query":{},"buildId":"KxFJgIacToE9f2wRQjZXR","nextExport":true,"autoExport":true,"isFallback":false,"scriptLoader":[]}'
        ) {
          console.log("[Browser]: login fail!");
        }
      }, 5000);
    });
    page.on("console", (msg) =>
      "[Browser]: login fail!" === msg.text() ? console.log(msg.text()) : null
    );
  });
