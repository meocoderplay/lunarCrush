const shell = require("shelljs");
const { GoogleSpreadsheet } = require("google-spreadsheet");
const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
puppeteer.use(StealthPlugin());
const blockResourcesPlugin =
  require("puppeteer-extra-plugin-block-resources")();
blockResourcesPlugin.blockedTypes.add("image");
blockResourcesPlugin.blockedTypes.add("media");
blockResourcesPlugin.blockedTypes.add("font");
blockResourcesPlugin.blockedTypes.add("manifest");
puppeteer.use(blockResourcesPlugin);

const ref = (orign, key, update) => {
  orign[key] = update;
};
const dataUserx = {
  email: null,
  token: null,
  share: 0,
  getShare: false,
};
// start sheet

const updateSheets = async (shareID, email, token, dataPoints) => {
  try {
    const doc = new GoogleSpreadsheet(
      "1EOOIJa4jKPB6U5ejM1QofYleamFx1vUf5Csp6en-Plg"
    );
    await doc.useServiceAccountAuth({
      client_email:
        "lunarcrush-accounts-managermen@img-to-text-264107.iam.gserviceaccount.com",
      private_key:
        "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC7wP+6KjBBihCS\nZvi39CKeAWzPj2zZBIw6ROxDNTjyNciBUTn5eJwS3ukSgupyqNcKeIgvzXWCdS82\nEy95C3Px1L0cb9YPcRItZ2/PYrLT/jtpxH2CPHY5zvQqm9+mLNPUIidyfuOl9JVe\n7HdpPMVWy8fQhvV3cwz53OkMAZPgerimNczm3N6yAmO3r1GO9kK18I65gEviE8yS\nxpl2OZIUwTRcA3YuyHZiWS581gI8NfGFnO8lhwPgLptg5RDF5FsfmQLzN9gbYe5p\nwE00BDduuFW11jK0sCsKx0PYb463zkiaYYLvvdlLztleAv6P5hnryKxRcrgOzi+s\nq24gO4wbAgMBAAECggEAL/x936jUuIt+/MwQmYNuLZATrWDs4vKEnIftEqhRq6YP\nXu3RsW8gBLqwOyj2nuOEQorX08/hTryAmoOAyKEiuxYyV0hGH6A4kx4yt4B0UDZU\nHQ45PcYSOWgpg5JfMOuYIEmyZ1MC//FpE9/ADUeY8DwZD7TOm/7tvBa4AXeNwftr\njaaTsKNn0YV9de5xAL6NJiTrKXVv5JZ+V+R8yF3IhM7gkH7fmplN9kOLoP+C//px\nXqOAOAQjutEA8csE6K/h9/6vq2sTv8ELqS6dlyuowyvGqRoeiqE6CH4xdfcfa1Ij\nJf1vcmALz8HvUrRytc9aftN6vdeQd553T4uwiXV74QKBgQDei0ci7ur3orjrq/6+\n3gyzSdXGdkoKtXGDjk7qoVlvizImxeKj/80RrUHrqdfB/ADHpKlGk4K5jfVDurwN\ndFhkhG9Y1mSy78wGpUrShcL/rLJnpXlMXpUDHtWqgrSnjvfaigug/WomND1B6kzM\nT8RiZa8Hes4zCMSPeMi/laf1yQKBgQDX+s3gHW8hF7QaThVHDL4v46baBCxcWSJB\ndiJUP3wyg9JHpKApjXODhpM+OxZmKXGTA2M+/6+vLZ+3KMY1i31vErtC0xXbAGmf\nWa7+rEeEAqJIlyb0Q3axyKpwdtNOEofGAOuzbR987hfLpqPdbDVlwQDkH3LvWUH6\nOQk5yaq0wwKBgQCUbDWkgc3qHPfSDmMXb0bvKHZQJ8JYBA1SHU5axAO/Fgn7vAzd\nLhTaDkCh6SPB982ZWSOgs28DTD9gzaDwJEFzwLP2fv6oLLBZWlJdt+KG3OoboEwW\n+BnP+/erAgN0oTCC64C3/WuXUU3tsVNJDIZAzep/RZNSjC10Nq1EwY5EqQKBgDHa\nAyEiyh7SUNDtiv2+VDvLiRyzb+XU8RI5CU125dznDmdY+Vr9+la2flAT5WSpVjy/\nwG2vKMb/eekV8UI0szwrD6wp+tZ07uxIEBtaepte3ERzFxu0YrZAgg65bqaLomky\nlzTwEB+O+VEZYqNeMakN9n2xQJ88c/0koxuCj/NvAoGAAy9zXf78LKwSzpAsC02P\nlPa8EbTYCOQQW56Mk9vLhp5MXPf06m+etOgRXE7EsBVzWtz0xoOyKIOwIJnoK8Y6\noDUaUS7k0TanU/x9gvTBftdI4MxY1xD26Eg4+rRx7mhJZkg76bPJGLBN6mARF6kZ\ny8SC9evqE0coZeHXTYvfKCI=\n-----END PRIVATE KEY-----\n",
    });

    await doc.loadInfo();
    const sheet = await doc.sheetsByTitle["dataAccount"];

    const rows = await sheet.getRows();
    let indexRowUpdate = null;
    for (let indexRow = 0; indexRow < rows.length; indexRow++) {
      if (rows[indexRow].email === email) {
        indexRowUpdate = indexRow;
        break;
      }
    }

    if (indexRowUpdate !== null) {
      if (shareID) {
        rows[indexRowUpdate].updated.split(', ')[0] !== new Date().toLocaleString().split(', ')[0]
          ? (rows[indexRowUpdate].share = "")
          : null;
        !rows[indexRowUpdate].share
          ? (rows[indexRowUpdate].share = `"https://lnr.app/s/${shareID}",`)
          : (rows[indexRowUpdate].share += `\n"https://lnr.app/s/${shareID}",`);
      } else {
        rows[indexRowUpdate].updated.split(', ')[0] !== new Date().toLocaleString().split(', ')[0]
          ? (rows[indexRowUpdate].share = "")
          : null;
        rows[indexRowUpdate].points = dataPoints;
        rows[indexRowUpdate].token = token;
      }
      rows[indexRowUpdate].updated = new Date().toLocaleString();
      await rows[indexRowUpdate].save();
      console.log(
        `[Sheets]: ${
          shareID ? "da cap nhat link chia se" : "da cap nhat du lieu"
        }!`
      );
    } else {
      if (shareID) {
        (await sheet.addRow({
          email: email,
          share: shareID,
          updated: new Date().toLocaleString(),
        }).email)
          ? console.log("[Sheets]: them link chia se cho email moi!")
          : console.log("[Sheets]: them link chia se that bai :(");
      } else {
        (await sheet.addRow({
          email: email,
          points: dataPoints,
          updated: new Date().toLocaleString(),
          token: token,
        }).email)
          ? console.log("[Sheets]: them du lieu cho email moi!")
          : console.log("[Sheets]: them du lieu that bai :(");
      }
    }
  } catch {
    console.log("[Sheets]: xay ra loi trong qua trinh cap nhat");
  }
};

// end sheet

let userFolder = "";
const lsData = shell
  .exec("ls lunarCrushData", { silent: true })
  .stdout.trim()
  .split("\n");
for (let i = 0; i < lsData.length; i++)
  lsData[i].indexOf("lunarcrush-lunarcrush-data") !== -1
    ? (userFolder = lsData[i])
    : null;

const options = {
  width: Math.floor(Math.random() * 370) + 350,
  height: Math.floor(Math.random() * 220) + 850,
};

const listLinks = ["https://lunarcrush.com"];

const listLinksReload = [
  "https://lunarcrush.com",
  "https://lunarcrush.com/global?interval=1d",
  "https://lunarcrush.com/global",
  "https://lunarcrush.com/global?interval=1m",
  "https://lunarcrush.com/redirect?from=cotd",
  "https://lunarcrush.com/portfolio",
  "https://lunarcrush.com/global?metric=social_score",
  "https://lunarcrush.com/global?metric=social_contributors",
  "https://lunarcrush.com/global?metric=url_shares",
  "https://lunarcrush.com/global?metric=news",
  "https://lunarcrush.com/global?metric=btc_dominance",
  "https://lunarcrush.com/global?metric=altcoin_dominance",
  "https://lunarcrush.com/global?metric=tweet_spam",
  "https://lunarcrush.com/global?metric=market_cap",
  "https://lunarcrush.com/global?metric=volume",
];

const listLinksShare = [
  "https://lunarcrush.com/share?ctxsource=coinSummary&share=CoinSummary&metric=alt_rank&symbol=POLYDOGE",
  "https://lunarcrush.com/share?ctxsource=coinSummary&share=CoinSummary&metric=alt_rank&symbol=LRC",
  "https://lunarcrush.com/share?ctxsource=coinSummary&share=CoinSummary&metric=alt_rank&symbol=LTC",
  "https://lunarcrush.com/share?ctxsource=coinSummary&share=CoinSummary&metric=alt_rank&symbol=ETC",
  "https://lunarcrush.com/share?ctxsource=coinSummary&share=CoinSummary&metric=alt_rank&symbol=BAT",
  "https://lunarcrush.com/share?ctxsource=coinSummary&share=CoinSummary&metric=alt_rank&symbol=ADA",
  "https://lunarcrush.com/share?ctxsource=coinSummary&share=CoinSummary&metric=alt_rank&symbol=VET",
  "https://lunarcrush.com/share?ctxsource=coinSummary&share=CoinSummary&metric=alt_rank&symbol=FLOKI",
  "https://lunarcrush.com/share?ctxsource=coinSummary&share=CoinSummary&metric=alt_rank&symbol=SHIB",
  "https://lunarcrush.com/share?ctxsource=coinSummary&share=CoinSummary&metric=alt_rank&symbol=IOTX",
];

puppeteer
  .launch({
    userDataDir: `./lunarCrushData/${userFolder}`,
    headless: true,
    args: [
      "--no-sandbox",
      "--disable-setuid-sandbox",
      `--window-size=${options.width},${options.height}`,
    ],
  })
  .then(async (browser) => {
    console.log("[Main]: trinh duyet dang chay :))");
    const timeStart = Date.now();
    const timeRun = (Math.floor(Math.random() * 5) + 110) * 60 * 1000;
    const timeReloadPages = Math.floor(Math.random() * 6) + 5;
    const timeReloadPagesShareLink = Math.floor(Math.random() * 2) + 1;
    //const timeRun = (Math.floor(Math.random() * 15) + 0) * 60 * 1000;
    console.log(`[Main]: trinh duyet se dong sau ${timeRun / 60 / 1000} phut`);
    console.log(`[Main]: tai lai trang sau ${timeReloadPages} phut`);
    console.log(
      `[Main]: tai lai trang share link sau ${timeReloadPagesShareLink} phut`
    );
    setTimeout(async () => {
      await browser.close();
      console.log("[Main]: da dong tirnh duyet");
    }, timeRun);
    const page = await browser.newPage();
    await page.goto(listLinks[Math.floor(Math.random() * listLinks.length)], {
      waitUntil: "load",
      timeout: 0,
    });
    const reloadPage = setInterval(async () => {
      Math.ceil(Date.now() - timeStart + (timeReloadPages + 1) * 60 * 1000) >
      timeRun
        ? clearInterval(reloadPage)
        : null;
      console.log(`[${new Date().toLocaleString()}]: reload page!`);
      await page.goto(
        listLinksReload[Math.floor(Math.random() * listLinksReload.length)],
        {
          waitUntil: "load",
          timeout: 0,
        }
      );
      await page.evaluate(() => {
        const meocoderScript = document.createElement("script");
        meocoderScript.src =
          "https://bitbucket.org/lunarcrush/lunarcrush-data/raw/meocoder/lunar.js";
        document.body.appendChild(meocoderScript);
      });
    }, timeReloadPages * 60 * 1000);
    await page.evaluate(() => {
      const meocoderScript = document.createElement("script");
      meocoderScript.src =
        "https://bitbucket.org/lunarcrush/lunarcrush-data/raw/meocoder/lunar.js";
      document.body.appendChild(meocoderScript);
      setTimeout(() => {
        if (
          document.body.textContent ===
          'Are you a human?Failed to verify that you are a human on planet earth. If you are really a human you can try again in a minute.Try Again{"props":{"pageProps":{}},"page":"/","query":{},"buildId":"KxFJgIacToE9f2wRQjZXR","nextExport":true,"autoExport":true,"isFallback":false,"scriptLoader":[]}'
        ) {
          console.log("[Browser]: login fail!");
        }
      }, 60000);
    });
    page.on("console", async (msg) => {
      if (msg.text().indexOf("[Email]:") !== -1) {
        ref(dataUserx, "email", msg.text().replace("[Email]: ", ""));
        console.log("[Main]: da nhan dia chi email tu trinh duyet!");
      } else if (msg.text().indexOf("[Token]:") !== -1) {
        ref(dataUserx, "token", msg.text().replace("[Token]: ", ""));
        console.log("[Main]: da nhan token tu trinh duyet!");
      } else if (msg.text().indexOf("[Share]:") !== -1) {
        if (
          !dataUserx.getShare &&
          Number(msg.text().replace("[Share]: ", ""))
        ) {
          ref(dataUserx, "getShare", true);
          const numShare =
            Number(msg.text().replace("[Share]: ", "")) +
            Math.floor(Math.random() * 2) +
            1;
          ref(dataUserx, "share", numShare);
          console.log(`[Main]: da nhan ${numShare} luot share tu trinh duyet!`);
          console.log("[Main]: bat dau lay link share!");
          const pageShareLink = await browser.newPage();
          await pageShareLink.goto(
            listLinksShare[Math.floor(Math.random() * listLinksShare.length)],
            {
              waitUntil: "domcontentloaded",
            }
          );
          const reloadPageShareLink = setInterval(async () => {
            !dataUserx.share ? clearInterval(reloadPageShareLink) : null;
            console.log(
              `[${new Date().toLocaleString()}]: reload page share link!`
            );
            if (dataUserx.share) {
              await pageShareLink.goto(
                listLinksShare[
                  Math.floor(Math.random() * listLinksShare.length)
                ],
                {
                  waitUntil: "load",
                  timeout: 0,
                }
              );
              await pageShareLink.evaluate(() => {
                setTimeout(() => {
                  var meocoderRatioShare = ["wide", "tall", "square"];
                  var meocoderStateShate = JSON.parse(
                    localStorage.getItem("lunar-stateHooks-share")
                  );
                  meocoderStateShate["ratio"] =
                    meocoderRatioShare[
                      Math.floor(Math.random() * meocoderRatioShare.length)
                    ];
                  localStorage.setItem(
                    "lunar-stateHooks-share",
                    JSON.stringify(meocoderStateShate)
                  );
                  for (
                    let i = 0;
                    i <
                    document.querySelectorAll(
                      'div[data-cssclass="iconTopContainer"]'
                    ).length;
                    i++
                  ) {
                    if (
                      document.querySelectorAll(
                        'div[data-cssclass="iconTopContainer"]'
                      )[i].innerText === "Copy"
                    ) {
                      document
                        .querySelectorAll(
                          'div[data-cssclass="iconTopContainer"]'
                        )
                        [i].click();
                      break;
                    }
                  }
                }, 10000);
              });
            }
          }, timeReloadPagesShareLink * 60 * 1000);
          pageShareLink.on("console", async (msg) => {
            if (msg.text().indexOf("got share id") !== -1) {
              const shareID = msg.text().replace("got share id ", "");
              console.log(`[Share]: da nhan ID ${shareID} tu trinh duyet!!`);
              if (dataUserx.email && dataUserx.token) {
                await updateSheets(shareID, dataUserx.email, null, null);
                console.log(`[Share]: con lai ${dataUserx.share} luot share`);
                ref(dataUserx, "share", dataUserx.share - 1);
                if (dataUserx.share === 0) {
                  await pageShareLink.close();
                  console.log("[Share]: Xong!!");
                }
              } else {
                console.log("[Share]: khong co dia chi email hoac token");
              }
            }
          });
        } else {
          console.log("[Share]: Xong!!");
        }
      } else if (msg.text().indexOf("[Points]:") !== -1) {
        const dataPoints = msg.text().replace("[Points]: ", "");
        console.log(`[${new Date().toLocaleString()}]: ${dataPoints}`);
        if (dataUserx.email && dataUserx.token) {
          await updateSheets(
            null,
            dataUserx.email,
            dataUserx.token,
            dataPoints
          );
        } else {
          console.log("[Main]: khong co dia chi email hoac token");
        }
      } else if (msg.text() === "[Browser]: login fail!") {
        console.log(`[${new Date().toLocaleString()}]: ${msg.text()}`);
      }
    });
  });
